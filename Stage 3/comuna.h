#ifndef COMUNA_H
#define COMUNA_H
#include "pedestrian.h"
#include <QRect>
#include <iostream>
#include <string>
#include <QtCharts>

using namespace QtCharts;
QT_USE_NAMESPACE

using namespace std;
class Comuna {
private:
    Pedestrian * pPerson;
    vector<Pedestrian*>* poblacion;
    double distancia;
    double p0,p1,p2;
    double simulationTime;
    QRect territory; // Alternatively: double width, length;
    // but more methods would be needed.
    QLineSeries *seriesLS;
    QLineSeries *seriesLI;
    QLineSeries *seriesLR;

public:
    Comuna(double distancia, double p0, double p1, double p2 , QLineSeries *seriesLS, QLineSeries *seriesLI, QLineSeries *seriesLR, double width=1000, double length=1000);
    double getWidth() const;
    double getHeight() const;
    void setPoblacion(vector<Pedestrian*>* poblacion);
    void computeNextState (double delta_t);
    void updateState ();
    static string getStateDescription();
    string getState() const;
    void updateGraph();
 };

#endif // COMUNA_H
