#include "comuna.h"
#include <iostream>
Comuna::Comuna(double distancia, double p0, double p1, double p2, QLineSeries *seriesLS, QLineSeries *seriesLI, QLineSeries *seriesLR, double width, double length): territory(0,0,width,length){
    pPerson=NULL;
    this->distancia = distancia;
    this->simulationTime = 0;
    this->p0 = p0;
    this->p1 = p1;
    this->p2 = p2;
    this->poblacion = NULL;
    this->seriesLI = seriesLI;
    this->seriesLR = seriesLR;
    this->seriesLS = seriesLS;
}
double Comuna::getWidth() const {
    return this->territory.width();
}
double Comuna::getHeight() const {
    return this->territory.height();
}
void Comuna::setPoblacion(vector<Pedestrian*>* poblacion){
    this->poblacion = poblacion;
}
void Comuna::computeNextState (double delta_t) {
    simulationTime += delta_t;
    for (unsigned long long i=0; i<poblacion->size(); i++ ){
                poblacion->at(i)->computeNextState(delta_t);
                for(unsigned long long j=(i+1); j<poblacion->size(); j++){
                    if(poblacion->at(i)->contactoEstrecho(poblacion->at(j), distancia)){
                        Pedestrian::exposicion(poblacion->at(j),poblacion->at(i), p0, p1, p2);//para cada individuo se verifica si ha tenido contacto estrecho con cualquiera de los otros individuos
                    }
                }
            }
}
void Comuna::updateState () {
    for(unsigned long long i=0; i<poblacion->size(); i++){
                poblacion->at(i)->updateState();
            }
}
string Comuna::getStateDescription(){
    return "I\t;S\t;R";
}
string Comuna::getState() const{
    int I=0;
    int S=0;
    int R=0;
    for(unsigned long long i=0; i<poblacion->size();i++){
        I+= poblacion->at(i)->infectado() ? 1:0;//se suma uno si el individuo esta infectado/ susceptible/ recuperado
        S+= poblacion->at(i)->suceptible() ? 1:0;
        R+= poblacion->at(i)->recuperado() ? 1:0;
    }
    string output = to_string(I)+";\t"+to_string(S)+";\t"+to_string(R);
    return output;
}

void Comuna::updateGraph(){
    double I=0;
    double S=0;
    double R=0;
    for(unsigned long long i=0; i<poblacion->size();i++){
        I+= poblacion->at(i)->infectado() ? 1:0;//se suma uno si el individuo esta infectado/ susceptible/ recuperado
        S+= poblacion->at(i)->suceptible() ? 1:0;
        R+= poblacion->at(i)->recuperado() ? 1:0;
    }
    seriesLS->append(qreal(simulationTime*0.01),qreal(S+I+R/poblacion->size()));
    seriesLI->append(qreal(simulationTime*0.01),qreal(I+R/poblacion->size()));
    seriesLR->append(qreal(simulationTime*0.01),qreal(R/poblacion->size()));
}
