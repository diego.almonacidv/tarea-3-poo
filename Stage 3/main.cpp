#include "mainwindow.h"
#include "comuna.h"
#include "simulator.h"
#include "pedestrian.h"
#include <QApplication>
#include <QtWidgets>
#include <QtCharts>
#include <iostream>
#include <fstream>
QT_USE_NAMESPACE
using namespace std;
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //Lectura de datos
    if (argc != 2){
        cout << "Usage: stage1 <configurationFile.txt>" << endl;
        cout << argc<<endl;
        exit(-1);
    }
    ifstream fin(argv[1]);
    double N, I, I_time, comunaWidth,comunaLength,speed, delta_t,
            deltaAngle, distancia,mascarillas, p0, p1, p2;
    // this is not really needed, just to check data read.
    cout << "File: " << argv[1] << endl;
    fin >> N >> I >> I_time;
    fin >> comunaWidth >> comunaLength;
    fin >> speed >> delta_t >> deltaAngle;
    fin >> distancia >> mascarillas >> p0 >> p1 >> p2;
    double samplingTime = 2.0;

    //creacion de interfaz grafica

    QLineSeries *seriesLS = new QLineSeries();
    QLineSeries *seriesLI = new QLineSeries();
    QLineSeries *seriesLV = new QLineSeries();
    QLineSeries *seriesLR = new QLineSeries();


    QAreaSeries *seriesS = new QAreaSeries(seriesLS);
    QAreaSeries *seriesI = new QAreaSeries(seriesLI);
    QAreaSeries *seriesV = new QAreaSeries(seriesLV);
    QAreaSeries *seriesR = new QAreaSeries(seriesLR);
    seriesS->setName("Susceptibles");
    seriesI->setName("Infectados");
    seriesV->setName("Vacunados");
    seriesR->setName("Recuperados");


    QPen penS(0x0683f6);
    seriesS->setPen(penS);
    QPen penI(0xf60606);
    seriesI->setPen(penI);
    QPen penV(0x4ce100);
    seriesV->setPen(penV);
    QPen penR(0xb89500);
    seriesR->setPen(penR);


    QLinearGradient gradientS(QPointF(0, 0), QPointF(0, 1));
    gradientS.setColorAt(0.0, 0x449ef0);
    gradientS.setColorAt(1.0, 0x0683f6);
    gradientS.setCoordinateMode(QGradient::ObjectBoundingMode);
    seriesS->setBrush(gradientS);

    QLinearGradient gradientI(QPointF(0, 0), QPointF(0, 1));
    gradientI.setColorAt(0.0, 0xFF1313);
    gradientI.setColorAt(1.0, 0xf60606);
    gradientI.setCoordinateMode(QGradient::ObjectBoundingMode);
    seriesI->setBrush(gradientI);

    QLinearGradient gradientV(QPointF(0, 0), QPointF(0, 1));
    gradientV.setColorAt(0.0, 0x4ce100);
    gradientV.setColorAt(1.0, 0x4ce100);
    gradientV.setCoordinateMode(QGradient::ObjectBoundingMode);
    seriesV->setBrush(gradientV);

    QLinearGradient gradientR(QPointF(0, 0), QPointF(0, 1));
    gradientR.setColorAt(0.0, 0xb89500);
    gradientR.setColorAt(1.0, 0xb89500);
    gradientR.setCoordinateMode(QGradient::ObjectBoundingMode);
    seriesR->setBrush(gradientR);


    QChart *chart = new QChart();
    chart->addSeries(seriesS);
    chart->addSeries(seriesI);
    chart->addSeries(seriesV);
    chart->addSeries(seriesR);
    chart->setTitle("Pandemic Evolution");
    QValueAxis* xAxis = new QValueAxis();
    QValueAxis* yAxis = new QValueAxis();
    xAxis->setRange(qreal(0),qreal(100));
    yAxis->setRange(qreal(0),qreal(100));
    xAxis->setTickCount(5);
    yAxis->setTickCount(5);
    xAxis->setLabelFormat("%.2f");
    yAxis->setLabelFormat("%.2f");

    chart->addAxis(yAxis,Qt::AlignLeft);
    chart->addAxis(xAxis,Qt::AlignBottom);
    xAxis->setMin(0);
    yAxis->setMin(0);
    xAxis->setMax(100);
    yAxis->setMax(100);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    //Simulacion
    Comuna comuna(distancia, p0 ,p1, p2, seriesLS , seriesLI , seriesLR , comunaWidth, comunaLength);
    Simulator sim(cout, comuna, delta_t, samplingTime, speed, deltaAngle, I_time, I, N, mascarillas);
    MainWindow w;
    w.setSim(&sim);
    w.setCentralWidget(chartView);
    w.resize(800, 500);
    w.show();

    return a.exec();
}
