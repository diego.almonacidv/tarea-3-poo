#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QKeyEvent>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionStart_triggered()
{
    sim->startSimulation();
}


void MainWindow::on_actionStop_triggered()
{
    sim->timer->stop();
}

void MainWindow::setSim(Simulator* sim){
    this->sim = sim;
}

void MainWindow::keyPressEvent(QKeyEvent *e){
    /*if(e->key() == Qt::Key_Left){     Ninguno metodo detecta las flechas.
        sim->samplingTime *= 2;
    }if(e->key() == Qt::Key_Right){
        sim->samplingTime /= 2;
    }*/
    /*if(e->key() == Qt::LeftArrow){
        sim->samplingTime *= 2;
    }if(e->key() == Qt::RightArrow){
        sim->samplingTime /= 2;
    }*/

}
