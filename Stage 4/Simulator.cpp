#include "Simulator.h"
Simulator::Simulator(ostream &output, Comuna &com, double delta, double speed,
                     double deltaAngle, double infectadoLimitTime, int infectados,
                     int individuos, double mascarillas):comuna(com),out(output){
    t=0;
    this->individuos = individuos;
    this->infectados = infectados;
    this->mascarillas = mascarillas;
    delta_t=delta;
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(simulateSlot()));
    this->deltaAngle = deltaAngle;
    this->speed = speed;
    this->infectadoLimitTime = infectadoLimitTime;
}
Simulator::~Simulator(){
    delete timer;
}
void Simulator::printStateDescription() const {
    string s=  "t\t" +comuna.getStateDescription();
    out << s << endl;
}
void Simulator::printState(double t) const{
    string s = to_string(t) + ";" + comuna.getState();
    out << s << endl;
}
void Simulator::startSimulation(){
    poblacionBuilder(parameters::N,parameters::I, mascarillas);
    comuna.vacunatoriosBuilder();
    comuna.restartGraph();
    comuna.restartTime();
    printStateDescription();
    t=0;
    printState(t);
    timer->start(parameters::samplingTime*1000);
}
void Simulator::simulateSlot(){
    double nextStop=t+parameters::samplingTime;
    comuna.updateGraph();
    while(t<nextStop) {
       comuna.computeNextState(delta_t); // compute its next state based on current global state
       comuna.updateState();  // update its state
       t+=delta_t;
    }
    printState(t);
}
void Simulator::poblacionBuilder(int N, int I, double mascarillas){
    poblacion = new vector<Pedestrian*>();
    double peopleWithMask = mascarillas * N;
    for(int i=0; i<N; i++){
        if(i < I && i< peopleWithMask){
            poblacion->push_back(new Pedestrian(this->comuna, this->speed, this->deltaAngle, 'i', infectadoLimitTime, true));//introduce personas infectadas con mascarillas
        } if(i < I && i >= peopleWithMask){
            poblacion->push_back(new Pedestrian(this->comuna, this->speed, this->deltaAngle, 'i', infectadoLimitTime, false));//introduce personas infectadas sin mascarillas
        } if(i >= I && i < peopleWithMask){
            poblacion->push_back(new Pedestrian(this->comuna, this->speed, this->deltaAngle, 's', infectadoLimitTime, true));//introduce personas susceptibles con mascarillas
        } if(i >= I && i >= peopleWithMask){
            poblacion->push_back(new Pedestrian(this->comuna, this->speed, this->deltaAngle, 's', infectadoLimitTime, false));//introduce personas susceptibles sin mascarillas
        }
     }
     this->comuna.setPoblacion(poblacion);
}
