#ifndef VACUNATORIO_H
#define VACUNATORIO_H
#include "Comuna.h"
#include <QRect>

class Vacunatorio
{
private:
    double vacSize,SemiLenght,x,y;
    static double VacTimeLimit,VacTimeCounter;
    QRect edificio;
    Comuna &comuna;
    bool alreadyBuild;

public:
    static bool active;
    Vacunatorio(Comuna &comuna, double vactimelimit, double vacSize);
    bool insideInNextState(Pedestrian individuo);
    void makeBuilding();
    bool isAlreadyBuild();
    static void increaseVacTimeCounter(double delta_t);
    static void computeNextState(double delta_t);
    static bool isActive();
};

#endif // VACUNATORIO_H
