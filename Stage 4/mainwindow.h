#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "Simulator.h"
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setSim(Simulator* sim);
    void keyReleaseEvent(QKeyEvent *e);
private slots:
    void on_actionStart_triggered();

    void on_actionStop_triggered();


    void on_actionParameters_triggered();

private:
    Ui::MainWindow *ui;
    Simulator* sim;
};
#endif // MAINWINDOW_H
