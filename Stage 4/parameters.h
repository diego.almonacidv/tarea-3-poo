#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <QDialog>

namespace Ui {
class parameters;
}

class parameters : public QDialog
{
    Q_OBJECT

public:
    static double N;
    static double I;
    static double I_time;
    static double samplingTime;
    explicit parameters(QWidget *parent = nullptr);
    ~parameters();

private slots:
    void on_pushButton_clicked();

private:
    Ui::parameters *ui;
};

#endif // PARAMETERS_H
