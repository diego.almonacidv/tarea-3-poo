#include "vacunatorio.h"
#include "Simulator.h"
#include "Pedestrian.h"
double Vacunatorio::VacTimeCounter;
double Vacunatorio::VacTimeLimit;
bool Vacunatorio::active = false;
using namespace std;
Vacunatorio::Vacunatorio(Comuna &comuna, double vactimelimit, double vacSize):comuna(comuna){
    this->vacSize = vacSize;
    VacTimeLimit = vactimelimit;
    VacTimeCounter = 0;
    this->x = (vacSize/2)+ (comuna.getWidth()-(vacSize/2))* Pedestrian::myRand .generateDouble();
    this->y = (vacSize/2)+ (comuna.getHeight()-(vacSize/2))* Pedestrian::myRand .generateDouble();
    this->SemiLenght = vacSize/2;
    active = false;
    cout<<"VactimeLimit: " <<VacTimeLimit<<endl;
}

bool Vacunatorio::insideInNextState(Pedestrian individuo){
    if((individuo.getX_tPlusDelta() <= x+SemiLenght) && (individuo.getX_tPlusDelta() >= x-SemiLenght) && (individuo.getY_tPlusDelta() <= y+SemiLenght) && (individuo.getY_tPlusDelta() >= y-SemiLenght)){
        return true;
    }else{
        return false;
    }
}



void Vacunatorio::computeNextState(double delta_t){
    if (! isActive()){
        increaseVacTimeCounter(delta_t);
    }
}

void Vacunatorio::increaseVacTimeCounter(double delta_t){
    VacTimeCounter += delta_t;
    if(VacTimeCounter >= VacTimeLimit){
        active = true;
    }
}

bool Vacunatorio::isActive(){return active;}
