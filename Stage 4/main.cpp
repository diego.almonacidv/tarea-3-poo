#include "mainwindow.h"
#include "Comuna.h"
#include "Simulator.h"
#include "Pedestrian.h"
#include "parameters.h"

#include <QApplication>
#include <QtWidgets>
#include <QtCharts>

#include <iostream>
#include <fstream>

QT_USE_NAMESPACE
using namespace std;
double parameters::samplingTime;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if (argc != 2){
        cout << "Usage: stage1 <configurationFile.txt>" << endl;
        cout << argc<<endl;
        exit(-1);
    }
    ifstream fin(argv[1]);
    double comunaWidth,comunaLength,speed, delta_t,
            deltaAngle, distancia,mascarillas, p0, p1, p2,
            NumVac, VacSize, VacTime;

    cout << "File: " << argv[1] << endl;
    fin >> parameters::N >> parameters::I >> parameters::I_time;
    fin >> comunaWidth >> comunaLength;
    fin >> speed >> delta_t >> deltaAngle;
    fin >> distancia >> mascarillas >> p0 >> p1 >> p2;
    fin >> NumVac >> VacSize >> VacTime;
    cout << parameters::N << "," << parameters::I << "," << parameters::I_time << endl;
    cout << comunaWidth <<"," << comunaLength << endl;
    cout << speed <<","<< delta_t <<"," << deltaAngle<< endl;
    cout << distancia <<","<< mascarillas <<"," << p0 << "," << p1 << "," << p2 << endl;
    cout << NumVac <<","<< VacSize <<","<< VacTime << endl;
    parameters::samplingTime = 2.0;

    QLineSeries *seriesLS = new QLineSeries();
    QLineSeries *seriesLI = new QLineSeries();
    QLineSeries *seriesLV = new QLineSeries();
    QLineSeries *seriesLR = new QLineSeries();

    QAreaSeries *seriesS = new QAreaSeries(seriesLS);
    QAreaSeries *seriesI = new QAreaSeries(seriesLI);
    QAreaSeries *seriesV = new QAreaSeries(seriesLV);
    QAreaSeries *seriesR = new QAreaSeries(seriesLR);
    seriesS->setName("Susceptibles");
    seriesI->setName("Infectados");
    seriesV->setName("Vacunados");
    seriesR->setName("Recuperados");


    QPen penS(0x0683f6);
    seriesS->setPen(penS);
    QPen penI(0xf60606);
    seriesI->setPen(penI);
    QPen penV(0x4ce100);
    seriesV->setPen(penV);
    QPen penR(0xb89500);
    seriesR->setPen(penR);


    QLinearGradient gradientS(QPointF(0, 0), QPointF(0, 1));
    gradientS.setColorAt(0.0, 0x449ef0);
    gradientS.setColorAt(1.0, 0x0683f6);
    gradientS.setCoordinateMode(QGradient::ObjectBoundingMode);
    seriesS->setBrush(gradientS);

    QLinearGradient gradientI(QPointF(0, 0), QPointF(0, 1));
    gradientI.setColorAt(0.0, 0xFF1313);
    gradientI.setColorAt(1.0, 0xf60606);
    gradientI.setCoordinateMode(QGradient::ObjectBoundingMode);
    seriesI->setBrush(gradientI);

    QLinearGradient gradientV(QPointF(0, 0), QPointF(0, 1));
    gradientV.setColorAt(0.0, 0x4ce100);
    gradientV.setColorAt(1.0, 0x4ce100);
    gradientV.setCoordinateMode(QGradient::ObjectBoundingMode);
    seriesV->setBrush(gradientV);

    QLinearGradient gradientR(QPointF(0, 0), QPointF(0, 1));
    gradientR.setColorAt(0.0, 0xb89500);
    gradientR.setColorAt(1.0, 0xb89500);
    gradientR.setCoordinateMode(QGradient::ObjectBoundingMode);
    seriesR->setBrush(gradientR);


    QChart *chart = new QChart();
    chart->addSeries(seriesS);
    chart->addSeries(seriesI);
    chart->addSeries(seriesV);
    chart->addSeries(seriesR);
    chart->setTitle("Pandemic Evolution");


    QValueAxis *axisX = new QValueAxis;
    QValueAxis *axisY = new QValueAxis;
    axisX->setRange(0, 100);
    axisY->setRange(0, parameters::N);
    axisX->setTickCount(5);
    axisY->setTickCount(5);
    axisX->setLabelFormat("%.2f");
    axisY->setLabelFormat("%.2f");
    chart->addAxis(axisX,Qt::AlignBottom);
    chart->addAxis(axisY,Qt::AlignLeft);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    Comuna comuna(distancia, NumVac, VacTime, VacSize,p0 ,p1, p2, seriesLS , seriesLI , seriesLR ,seriesLV, axisY ,comunaWidth, comunaLength);
    Simulator sim(cout, comuna, delta_t, speed, deltaAngle, parameters::I_time, parameters::I, parameters::N, mascarillas);

    MainWindow w;
    w.setSim(&sim);
    w.setCentralWidget(chartView);
    w.resize(800, 500);
    w.show();

    return a.exec();
}

