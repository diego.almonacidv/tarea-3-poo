#include "parameters.h"
#include "ui_parameters.h"
#include "Comuna.h"
double::parameters::N = 0;
double::parameters::I = 0;
double::parameters::I_time = 0;

parameters::parameters(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::parameters)
{
    ui->setupUi(this);
}

parameters::~parameters()
{
    delete ui;
}

void parameters::on_pushButton_clicked()
{
    if( ui->lineEdit->text()!=NULL && ui->lineEdit_2->text()!=NULL && ui->lineEdit_3->text()!=NULL ){
        parameters::N = (ui->lineEdit->text()).toDouble();
        parameters::I = (ui->lineEdit_2->text()).toDouble();
        parameters::I_time = (ui->lineEdit_3->text()).toDouble();
    close();
    }else{
        QMessageBox messageBox;
        messageBox.critical(0,"Error","Fill all arguments!");
        messageBox.setFixedSize(500,200);
    }
}
