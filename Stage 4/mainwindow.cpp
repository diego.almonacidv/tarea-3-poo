#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "parameters.h"
#include <QKeyEvent>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionStart_triggered()
{
    sim->startSimulation();
    ui->menuSettings->menuAction()->setVisible(false);
}

void MainWindow::on_actionStop_triggered()
{
    sim->timer->stop();
    ui->menuSettings->menuAction()->setVisible(true);
}

void MainWindow::setSim(Simulator* sim){
    this->sim = sim;
}

void MainWindow::on_actionParameters_triggered()
{
    parameters w;
    w.exec();
}

void MainWindow::keyReleaseEvent(QKeyEvent* e){
    if(e->key() == Qt::Key_Left){
        parameters::samplingTime /= (double)2.0;
    }if(e->key() == Qt::Key_Right){
        parameters::samplingTime *= (double)2.0;
    }

}

