#include "Comuna.h"
#include <iostream>
#include "vacunatorio.h"
#include "parameters.h"

Comuna::Comuna(double distancia, double NumVac,double VacTimeLimit,double VacSize, double p0, double p1, double p2,
               QLineSeries *seriesLS, QLineSeries *seriesLI, QLineSeries *seriesLR,QLineSeries *seriesLV,QValueAxis *axis, double width, double length): territory(0,0,width,length){
    pPerson=NULL;
    this->VacSize = VacSize;
    this->NumVac = NumVac;
    this->VacTimeLimit = VacTimeLimit;
    this->distancia = distancia;
    this->simulationTime = 0;
    this->p0 = p0;
    this->p1 = p1;
    this->p2 = p2;
    this->poblacion = NULL;
    this->seriesLI = seriesLI;
    this->seriesLR = seriesLR;
    this->seriesLS = seriesLS;
    this->seriesLV = seriesLV;
    this->axisY =axis;
    Vacunatorio::active = false;
}
double Comuna::getWidth() const {
    return this->territory.width();
}
double Comuna::getHeight() const {
    return this->territory.height();
}
void Comuna::setPoblacion(vector<Pedestrian*>* poblacion){
    this->poblacion = poblacion;
}
void Comuna::computeNextState (double delta_t) {
    simulationTime += delta_t;
    Vacunatorio::computeNextState(delta_t);
    for (unsigned long long i=0; i<poblacion->size(); i++ ){
        poblacion->at(i)->computeNextState(delta_t);
        vaccineLogic(i);
        for(unsigned long long j=(i+1); j<poblacion->size(); j++){
            if(poblacion->at(i)->contactoEstrecho(poblacion->at(j), distancia)){
                Pedestrian::exposicion(poblacion->at(j),poblacion->at(i), p0, p1, p2);//para cada individuo se verifica si ha tenido contacto estrecho con cualquiera de los otros individuos
            }
        }
    }
}
void Comuna::updateState () {
    for(unsigned long long i=0; i<poblacion->size(); i++){
                poblacion->at(i)->updateState();
            }
}
string Comuna::getStateDescription(){
    return "S\t;I\t;R\t;V";
}
string Comuna::getState() const{
    int I=0;
    int S=0;
    int R=0;
    int V=0;
    for(unsigned long long i=0; i<poblacion->size();i++){
        I+= poblacion->at(i)->infectado() ? 1:0;//se suma uno si el individuo esta infectado/ susceptible/ recuperado
        S+= poblacion->at(i)->suceptible() ? 1:0;
        R+= poblacion->at(i)->recuperado() ? 1:0;
        V+= poblacion->at(i)->vacunado() ? 1:0;
    }
    string output = to_string(S)+";\t"+to_string(I)+";\t"+to_string(R)+";\t"+to_string(V);
    return output;
}

void Comuna::updateGraph(){
    double I=0;
    double S=0;
    double R=0;
    double V=0;
    for(unsigned long long i=0; i<poblacion->size();i++){
        I+= poblacion->at(i)->infectado() ? 1:0;//se suma uno si el individuo esta infectado/ susceptible/ recuperado
        S+= poblacion->at(i)->suceptible() ? 1:0;
        R+= poblacion->at(i)->recuperado() ? 1:0;
        V+= poblacion->at(i)->vacunado() ? 1:0;
    }

    (*seriesLS).append(qreal(simulationTime/100), qreal((S+V+I+R)/poblacion->size()));
    (*seriesLI).append(qreal(simulationTime/100), qreal((I+R+V)/poblacion->size()));
    (*seriesLR).append(qreal(simulationTime/100), qreal(R/poblacion->size()));
    (*seriesLV).append(qreal(simulationTime/100), qreal((V+R)/poblacion->size()));
    cout << parameters::N << "," << parameters::I << "," << parameters::I_time <<endl;
}

void Comuna::vacunatoriosBuilder(){
    this->vacunatorios = new vector<Vacunatorio*>();
    for(int i=0; i< NumVac; i++){
        vacunatorios->push_back(new Vacunatorio(*this, VacTimeLimit, VacSize));
    }

}
void Comuna::vaccineLogic(int i){
    if(Vacunatorio::isActive()){
        if(poblacion->at(i)->suceptible()) {
            for (unsigned long long k = 0; k < vacunatorios->size(); k++) {
                if (vacunatorios->at(k)->insideInNextState(*poblacion->at(i))) {
                    poblacion->at(i)->vacunarse();
                }
            }
        }
    }
}

void Comuna::restartGraph(){
    seriesLS->clear();
    seriesLI->clear();
    seriesLR->clear();
    seriesLV->clear();
    axisY->setRange(0, parameters::N);
}

void Comuna::restartTime(){this->simulationTime = 0;}


