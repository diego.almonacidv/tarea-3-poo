#ifndef COMUNA_H
#define COMUNA_H
#include "Pedestrian.h"
class Vacunatorio;
#include <QRect>
#include <iostream>
#include <string>
#include <QtCharts>
#include "Simulator.h"

using namespace QtCharts;
QT_USE_NAMESPACE

using namespace std;
class Comuna {
private:
    Pedestrian * pPerson;
    vector<Pedestrian*>* poblacion;
    vector<Vacunatorio*>* vacunatorios;
    double distancia;
    double p0,p1,p2;
    double simulationTime;
    double NumVac, VacTimeLimit, VacSize;
    QRect territory;
    QLineSeries *seriesLS;
    QLineSeries *seriesLI;
    QLineSeries *seriesLR;
    QLineSeries *seriesLV;
    QValueAxis *axisY;

public:
    Comuna(double distancia, double NumVac, double VacTimeLimit,double VacSize, double p0, double p1, double p2 ,
           QLineSeries *seriesLS, QLineSeries *seriesLI, QLineSeries *seriesLR,QLineSeries *seriesLV, QValueAxis *axis,double width=1000, double length=1000);
    double getWidth() const;
    double getHeight() const;
    void setPoblacion(vector<Pedestrian*>* poblacion);
    void computeNextState (double delta_t);
    void updateState ();
    static string getStateDescription();
    string getState() const;
    void updateGraph();
    void vacunatoriosBuilder();
    void showVacunatorios();
    void vaccineLogic(int i);
    void restartTime();
    void restartGraph();
 };

#endif // COMUNA_H
