#include <QtMath> // for M_PI and functions, see https://doc.qt.io/qt-5/qtmath.html
#include <string>
#include "Comuna.h"
#include "Pedestrian.h"

Pedestrian::Pedestrian (Comuna &com, double speed, double deltaAngle):comuna(com)/*....*/{
    myRand = QRandomGenerator::securelySeeded();
    angle = M_PI*2*myRand.generateDouble();
    double width = comuna.getWidth();
    double height = comuna.getHeight();
    x = myRand.generateDouble()*width;
    y = myRand.generateDouble()*height;
    this->deltaAngle = deltaAngle;
    x_tPlusDelta = x;
    y_tPlusDelta = y;
    this->speed = speed*(0.9+0.2*myRand.generateDouble());
// .....
}
string Pedestrian::getState() const {
    string s=to_string(x) + ",\t";
    s+= to_string(y);
    return s;
}
void Pedestrian::computeNextState(double delta_t) {
    double r=myRand.generateDouble();
    angle+=deltaAngle*(1-2*r);
    x_tPlusDelta=x+speed*qCos(angle)*delta_t;
    y_tPlusDelta=y+speed*qSin(angle)*delta_t;
    if(x_tPlusDelta < 0){   // rebound logic: pared izquierda
        angle = M_PI - angle;
        x_tPlusDelta = -x_tPlusDelta;
    }
    else if( x_tPlusDelta > comuna.getWidth()){ // rebound logic: pared derecha
        x_tPlusDelta = 2* comuna.getWidth()-x_tPlusDelta;
        angle = M_PI - angle;
    }
    if(y_tPlusDelta < 0){   // rebound logic: pared inferior
        y_tPlusDelta = -y_tPlusDelta;
        angle = -angle;
    }
    else if( y_tPlusDelta > comuna.getHeight()){  // rebound logic: pared superior
        y_tPlusDelta = 2* comuna.getHeight()-y_tPlusDelta;
        angle = -angle;

    }
//...
}
void Pedestrian::updateState(){
    x=x_tPlusDelta;
    y=y_tPlusDelta;
}
