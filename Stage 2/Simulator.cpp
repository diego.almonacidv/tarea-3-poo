#include "Simulator.h"
Simulator::Simulator(ostream &output, Comuna &com,
                     double delta, double st, double speed,
                     double deltaAngle, double infectadoLimitTime,
                     int infectados, int individuos):comuna(com),out(output)/*.....*/{
    t=0;
    this->individuos = individuos;
    this->infectados = infectados;
    cout<<individuos<<endl;

    delta_t=delta;
    samplingTime=st;
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(simulateSlot()));
    this->deltaAngle = deltaAngle;
    this->speed = speed;
    this->infectadoLimitTime = infectadoLimitTime;
    poblacionBuilder(individuos,infectados);

}
Simulator::~Simulator(){
    delete timer;
}
void Simulator::printStateDescription() const {
    string s=  "t\t" +comuna.getStateDescription();//....
    out << s << endl;
}
void Simulator::printState(double t) const{
    string s = to_string(t) + ";" + comuna.getState();
    //...
    out << s << endl;
}
void Simulator::startSimulation(){
    printStateDescription();
    t=0;
    printState(t);
    timer->start(samplingTime*1000);
}
void Simulator::simulateSlot(){
    double nextStop=t+samplingTime;
    while(t<nextStop) {
       comuna.computeNextState(delta_t); // compute its next state based on current global state
       comuna.updateState();  // update its state
       t+=delta_t;
    }
    printState(t);
}
void Simulator::poblacionBuilder(int N, int I){
    poblacion = new vector<Pedestrian*>();
    for(int i=0; i<N; i++){

        if(i<I){
            poblacion->push_back(new Pedestrian(this->comuna, this->speed, this->deltaAngle, 'i', infectadoLimitTime));//se agregan los infectados
        }
        else{
            poblacion->push_back(new Pedestrian(this->comuna, this->speed, this->deltaAngle, 's', infectadoLimitTime));//se agregan los susceptibles
        }
     }
     this->comuna.setPoblacion(poblacion);
}
