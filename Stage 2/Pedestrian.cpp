#include <QtMath> // for M_PI and functions, see https://doc.qt.io/qt-5/qtmath.html
#include <string>
#include "Comuna.h"
#include "Pedestrian.h"
QRandomGenerator Pedestrian::myRand = QRandomGenerator::securelySeeded();
Pedestrian::Pedestrian (Comuna &com, double speed, double deltaAngle, char estado, double infectadoLimitTime):comuna(com)/*....*/{
    angle = Pedestrian::myRand.generateDouble()*2*M_PI;
    double width = comuna.getWidth();
    double largo = comuna.getHeight();
    this->x=Pedestrian::myRand.generateDouble()*width;
    this->y=Pedestrian::myRand.generateDouble()*largo;
    this->comuna = com;
    this->speed = speed;
    this->deltaAngle = deltaAngle;
    this->x_tPlusDelta = this->x;
    this->y_tPlusDelta = this->y;
    this->estado = estado;
    this->estado_tPlusDelta = estado;
    this->infectadoLimitTime = infectadoLimitTime;

    if(this->infectado()){
        initInfectadoTimer();
    }else{
        infectadoTime = -1;
    }


// .....
}
string Pedestrian::getState() const {
    string s=to_string(x) + ",\t";
    s+= to_string(y);
    return s;
}
void Pedestrian::computeNextState(double delta_t) {
    double r=Pedestrian::myRand.generateDouble();
    angle+=deltaAngle*(1-2*r);
    x_tPlusDelta=x+speed*qCos(angle)*delta_t;
    y_tPlusDelta=y+speed*qSin(angle)*delta_t;
    if(infectadoTime>=0){ //aumenta tiempo acumulado del infectado
        runInfectadoTime(delta_t);
    }
    if(x_tPlusDelta < 0){   // rebound logic: pared izquierda
        angle = M_PI - angle;
        x_tPlusDelta = -x_tPlusDelta;
    }
    else if( x_tPlusDelta > comuna.getWidth()){ // rebound logic: pared derecha
        x_tPlusDelta = 2* comuna.getWidth()-x_tPlusDelta;
        angle = M_PI - angle;
    }
    if(y_tPlusDelta < 0){   // rebound logic: pared inferior
        y_tPlusDelta = -y_tPlusDelta;
        angle = -angle;
    }
    else if( y_tPlusDelta > comuna.getHeight()){  // rebound logic: pared superior
        y_tPlusDelta = 2* comuna.getHeight()-y_tPlusDelta;
        angle = -angle;

    }
//...
}
double Pedestrian::getX(){return this->x;}
double Pedestrian::getY(){return this->y;}
double Pedestrian::distanceTo(Pedestrian* individuo){
    double xf = individuo->getX();
    double yf = individuo->getY();
    double dx = (xf - this->x);
    double dy = (yf-this->y);
    double distance =qSqrt(dx*dx + dy*dy);
    return distance;
}

bool Pedestrian::contactoEstrecho(Pedestrian* individuo, double d){
    if(distanceTo(individuo)<=d){
        return true;
    }else{
        return false;
    }
}
bool Pedestrian::infectado(){
    return this->estado=='i';
}

bool Pedestrian::suceptible(){
    return this->estado=='s';
}
bool Pedestrian::recuperado(){
    return this->estado=='r';
}
void Pedestrian::initInfectadoTimer(){
        infectadoTime=0;
}
void Pedestrian::exposicion(Pedestrian* individuo1, Pedestrian* individuo2, double P){//contagia susceptibles cuando existe contacto estrecho
        if(myRand.generateDouble() < P){
            if(individuo1->infectado() && individuo2->suceptible()){
                individuo2->estado_tPlusDelta = 'i';
            }
            if(individuo2->infectado() && individuo1->suceptible()){
                individuo1->estado_tPlusDelta = 'i';
            }
        }
    }
void Pedestrian::runInfectadoTime(double delta_t){//aumenta tiempo acumulado del infectado

    if(infectadoTime + delta_t < infectadoLimitTime) {
        infectadoTime += delta_t;
    }else{
        infectadoTime = -1;
        this->estado_tPlusDelta = 'r';
    }
}
void Pedestrian::updateState(){
    x=x_tPlusDelta;
    y=y_tPlusDelta;
    if((estado_tPlusDelta == 'i') && (infectadoTime ==-1)){
        initInfectadoTimer();
    }
    estado = estado_tPlusDelta;
}
