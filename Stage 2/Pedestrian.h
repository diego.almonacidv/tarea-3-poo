#ifndef PEDESTRIAN_H
#define PEDESTRIAN_H
#include <string>
#include <QRandomGenerator>
using namespace std;
class Comuna;
class Pedestrian {
private:
    double x, y, speed, angle, deltaAngle;
    double x_tPlusDelta, y_tPlusDelta;
    char estado, estado_tPlusDelta;
    double infectadoTime, infectadoLimitTime;
    Comuna &comuna;
    // see https://doc.qt.io/qt-5/qrandomgenerator.html

public:
    static QRandomGenerator myRand;
    Pedestrian(Comuna &com, double speed, double deltaAngle, char estado, double infectadoLimitTime);
    static string getStateDescription() {
        return "x, \ty";
    };
    string getState() const;
    void computeNextState(double delta_t);
    void updateState();
    void runInfectadoTime(double delta_t);
    static void exposicion(Pedestrian* individuo1, Pedestrian* individuo2, double P);
    void initInfectadoTimer();
    bool infectado();
    bool suceptible();
    bool recuperado();
    bool contactoEstrecho(Pedestrian* individuo, double d);
    double distanceTo(Pedestrian* individuo);
    double getX();
    double getY();
};

#endif // PEDESTRIAN_H
