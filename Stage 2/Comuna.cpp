#include "Comuna.h"
#include <iostream>
Comuna::Comuna(double distancia, double probabilidad,double width, double length): territory(0,0,width,length){
    pPerson=NULL;
    this->distancia = distancia;
    this->probabilidad = probabilidad;
    this->poblacion = NULL;
}
double Comuna::getWidth() const {
    return this->territory.width();
}
double Comuna::getHeight() const {
    return this->territory.height();
}
void Comuna::setPoblacion(vector<Pedestrian*>* poblacion){
    this->poblacion = poblacion;
}
void Comuna::computeNextState (double delta_t) {
    for (unsigned long long i=0; i<poblacion->size(); i++ ){
                poblacion->at(i)->computeNextState(delta_t);
                for(unsigned long long j=(i+1); j<poblacion->size(); j++){
                    if(poblacion->at(i)->contactoEstrecho(poblacion->at(j), distancia)){
                        Pedestrian::exposicion(poblacion->at(j),poblacion->at(i),probabilidad);//para cada individuo se verifica si ha tenido contacto estrecho con cualquiera de los otros individuos
                    }
                }
            }
}
void Comuna::updateState () {
    for(unsigned long long i=0; i<poblacion->size(); i++){
                poblacion->at(i)->updateState();
            }
}
string Comuna::getStateDescription(){
    return "I\t;S\t;R";
}
string Comuna::getState() const{
    int I=0;
    int S=0;
    int R=0;
    unsigned long long a = poblacion->size();
    for(unsigned long long i=0; i<poblacion->size();i++){
        I+= poblacion->at(i)->infectado() ? 1:0;//se suma uno si el individuo esta infectado/ susceptible/ recuperado
        S+= poblacion->at(i)->suceptible() ? 1:0;
        R+= poblacion->at(i)->recuperado() ? 1:0;
    }
    string output = to_string(I)+";\t"+to_string(S)+";\t"+to_string(R);
    return output;
}
