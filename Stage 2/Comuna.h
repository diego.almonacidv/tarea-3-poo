#ifndef COMUNA_H
#define COMUNA_H
#include "Pedestrian.h"
#include <QRect>
#include <iostream>
#include <string>
using namespace std;
class Comuna {
private:
    Pedestrian * pPerson;
    vector<Pedestrian*>* poblacion;
    double distancia;
    double probabilidad;
    QRect territory; // Alternatively: double width, length;
    // but more methods would be needed.

public:
    Comuna(double distancia, double probabilidad, double width=1000, double length=1000);
    double getWidth() const;
    double getHeight() const;
    void setPoblacion(vector<Pedestrian*>* poblacion);
    void computeNextState (double delta_t);
    void updateState ();
    static string getStateDescription();
    string getState() const;
 };

#endif // COMUNA_H
