## Simulando la Evolución de una Pandemia

## Tabla de información
***
1. [Información General](#Información-general)
2. [Tecnología](#Tecnologías)
3. [Ejecución del programa](#Ejecución-del-programa)
4. [Colaboraciones](#Colaboraciones)
***

### Información General

Saludos a la persona que esté leyendo este documento, este te ayudará a comprender
de mejor manera nuestro programa, te guiaremos en cada paso.

Este programa se compone de los siguientes archivos:

+ La carpeta "**Stage1**" Correspondiente a la etapa 1
+ La carpeta "**Stage2**" Correspondiente a la etapa 2
+ La carpeta "**Stage3**" Correspondiente a la etapa 3
+ La carpeta "**Stage4**" Correspondiente a la etapa 4


Dentro de cada carpeta Stage contiene sus respectivos archivos de **Headers** y **Sources** el archivo que contiene el 
*main* es llamado de la misma forma que su **Stage** correspondiente, además,
un archivo de texto llamado **"Config.txt"**

### Tecnologías
***
Una lista de tecnologías usadas en el proyecto.
* [Qt Creator](https://www.qt.io/home): Version 5.12.11
* [GitLab](https://gitlab.com/diego.almonacidv/tarea-3-poo): Version 13.12.0


## Ejecución del Programa
***
Para gestionar nuestro programa utilizamos un recurso llamado "GitLab" que con
algunos conocimientos de "GIT" puedes acceder fácilmente.
(De todos modos explicaremos paso a paso)

Para compilar nuestro programa primero tendrás que estar dentro de una carpeta
local y hacer un **Clone** desde nuestro repositorio GIT  con el siguiente:

```
$ git clone https://gitlab.com/diego.almonacidv/tarea-3-poo.git
```

Luego desde tu terminal acceder a la carpeta que acabas de **"clonar"** en este caso
se creará una carpeta con el nombre "tarea-3-poo"
```
$ cd ../tarea-3-poo
```

Una vez dentro para compilar el Stage que quieras evaluar y para ejecutar el programa necesitaras hacer uso de 
Qt Creator

Si lo tienes instalado en tu equipo, solo tienes que abrir el archivo **Stage.pro** correspondiente al stage a evaluar.

Una vez dentro, en la barra izquierda tiene que acceder a **Projects** para configurar y poder correr el programa como 
se puede ver en la siguiente imagen.

![Compilación](https://gitlab.com/diego.almonacidv/tarea-3-poo/-/blob/master/Recursos/Explicación_Compilación.PNG?raw=true "Compilación")

Luego en la parte inferior correr el programa de cada Stage.

## Colaboraciones
***

Integrantes del equipo:
+ Diego Almonacid
+ Ignacio Barrera
+ David Carrasco
+ Mariapaz Gomez

